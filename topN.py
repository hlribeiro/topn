import sys

# read file line by line to prevent memory exhaustion
def process_data(n, data_file):
  with open(data_file) as openfile:
    for line in openfile:
      if line.strip().isnumeric():
        line = int(line.strip())
        topN(n, line)

# fills the list first and sorts. Then check each number and replace it on the list if necessary. Discard if less than smaller number on the list.
def topN(n, line):
  if len(top_numbers) <= n:
    top_numbers.append(line)
    top_numbers.sort(reverse=True)
  elif line > top_numbers[n]:
    top_numbers[n] = line
    top_numbers.sort(reverse=True)
  else:
    pass

# main program
if __name__ == "__main__":
  n = int(sys.argv[1])-1
  data_file = sys.argv[2]
  top_numbers = []
  
  process_data(n, data_file)
  
  print('Here are the highest {} numbers on the file {}:'.format(n+1,data_file))
  print(*top_numbers, sep="\n")